'use strict';

import SettingsController from './settings.controller';

export default angular.module('designbidsComApp.settings', [])
  .controller('SettingsController', SettingsController)
  .name;
