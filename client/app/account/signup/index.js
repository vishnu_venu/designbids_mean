'use strict';

import SignupController from './signup.controller';

export default angular.module('designbidsComApp.signup', [])
  .controller('SignupController', SignupController)
  .name;
