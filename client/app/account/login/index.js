'use strict';

import LoginController from './login.controller';

export default angular.module('designbidsComApp.login', [])
  .controller('LoginController', LoginController)
  .name;
