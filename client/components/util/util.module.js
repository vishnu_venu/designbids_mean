'use strict';

import {
  UtilService
} from './util.service';

export default angular.module('designbidsComApp.util', [])
  .factory('Util', UtilService)
  .name;
