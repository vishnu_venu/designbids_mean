/**
 * Created by vishnu on 9/7/16.
 */
'use strict'
mongoose.promise = require('bluebird');
import mongoose, { Schema } from 'mongoose';
import { BidSchema } from './bid';

var ProjectSchema = new Schema({
  userId: { type: String , required: true},
  title: { type: String , required: true},
  description: { type: String , required: true},
  budget: { type: Number , required: true},
  location: { type: String },
  bids:[BidSchema],
  status: { type: String, default: 'AUDITING'}
},{
  timestamps: { createAt: 'created_at'}
});

/**
 * Pre-save Hook
 */

// if one of the bidder win other will automatically rejected
ProjectSchema
  .pre('save', function(next){
    // Handle if bid status modified
    if(!this.isModified('bids')){
      return next()
    }

    if( this.bids.length < 2){
      /**
       * we do'nt need to do anything as we have only one bid
       */
      return next();
    }

    // checking if we have any winner of this project
    let bid = this.bids.filter((bid)=>{ return bid.status === 'WON'})
    if( bid.length === 0 ){
      return next();
    } else{
      this.bids.map((bid)=>{
        if( bid.status !== 'WON'){
          bid.status = 'REJECTED'; // REJECT EVERYONE IN THE BIDDER LIST
        }
      })
    }
  })

/**
 * Virtual Functions
 * */

ProjectSchema
  .virtual('getBiddersCount')
  .get( ()=>{
    return { count: this.bids.length };
  })

ProjectSchema
  .virtual('getInterestedBids')
  .get( ()=>{
    return { bids: this.bids.filter((bid)=>{
      return bid.status === 'INTERESTED';
    })}
  })

ProjectSchema
  .virtual('getApprovedBids')
  .get( (bid)=>{
    return bid.status === 'APPROVED';
  })

/*
 * ProjectSchema Method
 * */

ProjectSchema.methods = {
  addInterestedBidder(bidder = {}, cb){
    if( !bidder === {} ){
      var newBidder = new Bid({
        bidderId: bidder.id,
        name: bidder.name,
        profileImage: bidder.image,
        contacts : bidder.number
      })
      this.bids.push(newBidder)
      this.save(cb)
    }
    cb( true, false);
  }
};


export default mongoose.model('Project', ProjectSchema);
