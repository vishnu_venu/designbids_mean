/**
 * Created by vishnu on 9/7/16.
 */
'use strict';

var express = require('express');
var controller = require('./project.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.delete('/:id', controller.destroy);


// Add to Interest
router.put('/:id/interested', controller.interested);
router.put('/:id/notinterested', controller.notInterested);
module.exports = router;
