/**
 * Created by vishnu on 9/7/16.
 */
/**
 *  GET        /api/projects         ->   index
 *  POST       /api/projects         ->   create
 *  GET        /api/projects/:id     ->   show
 *  DELETE     /api/projects/:id     ->   destroy
 *
 *  POST       /api/projects/:id/bid   ->    bid
 *  PUT        /api/projects/:id/bid/:bidId/ ->
 * */



'use strict';

import Project from './project.model';
import {
  removeEntity,
  handleEntityNotFound,
  respondWithResult ,
  patchUpdates,
  handleError } from '../../libs'


/**
 * GET Project List
 * todo : must be paginate
 * */

export function index(req, res){
  return Project.find({})
    .then(projects=>{
      res.status(200).json(projects)
    })
    .catch(handleError(res));
}

/**
 * Post a New Project
 * */

export function create(req, res, next){
  var newProject = new Project(req.body);
  newProject.save()
    .then(respondWithResult(res, 201))
    .catch(handleError(res));
}


/**
 *  Show the specific Project
 */
export function show(req, res, next){
  let projectId = req.params.id;
  return Project.findById(projectId).exec()
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}


/**
* Upsert the given project in the DB at the Specified ID
** */
export function upsert(req, res){
  if(req.body._id){
    delete req.body._id;
  }
  return Project.findOneAndUpdate( req.params.id, req.body, { upsert: true, setDefaultsOnInsert: true, runValidators: true}).exec()
    .then( respondWithResult(res))
    .catch(handleError(res))
}


/**
 *  Patch the change in project data
 * */
export function patch(req, res){
  if( req.body._id){
    delete req.body._id;
  }
  return Project.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(patchUpdates(res.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

/**
 *  Delete a project from the DB
 * */
export function destroy(req, res){
  return Project.findById(req.params.id).exec()
    .then( handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}

/***
 *  Post Add a Interested Designer
 */
export function interested(req, res) {
  var projectId = req.params.id,
    // constructing bid from request body
    bid = {
      bidderId: req.body.bidderId,
      name: req.body.name,
      profileImage: req.body.profileImage,
      contacts : req.body.contacts
    };

  Project.findOne({_id: projectId}, {}).exec()
    .then((project)=> {
      project.bids.push(bid); // pushing new bid to project
      return project.save(); // saving the project
    })
    .then(()=> {
      return Project.findOne({_id: projectId}, {bids: 1}).exec(); // send updated bid list
    })
    .then((bids)=> {
      res.status(200).json(bids);
    })
    .catch((err)=> {
      res.status(500).json({err: err.message})
    })
   }
/**
 *  Post Remove a Interested Designer
 * */
export function notInterested(req, res){
  var projectId = req.params.id,
      bidId = req.body.bidderId;
  Project.findByIdAndUpdate(projectId, { $pull : { bids: { bidderId: bidId}}}).exec()
    .then((project) => { return project.save() })
    .then(()=>{ return Project.findOne({_id: projectId},{ bids: 1}).exec() })
    .then((bids)=>{ res.status(200).json(bids) })
    .catch(err => res.status(200).json({err: err}))
}
