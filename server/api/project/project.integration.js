'use strict';

var app = require('../..');
import request from 'supertest';

var newProject;

describe('Project API', function(){
  describe('GET /api/projects', function(){
    var projects;

    beforeEach( function(done){
      request(app)
        .get('/api/projects')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res)=> {
         if(err){  return done(err); }
          projects = res.body;
          done();
        });
    });

    it('should respond with JSON array', function(){
      projects.should.be.instanceOf(Array);
    });
  });

  describe('POST /api/projects', function(){
    beforeEach(function(done){
      request(app)
        .post('/api/projects/')
        .send({
          userId: '3445ddegghhr',
          title: 'test title',
          description: 'some test description',
          budget: 55555,
          location: 'Bangalore'})
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res)=>{
          if(err){
            return done(err);
          }
          newProject = res.body;
          done();
        });
    });

    it('should respond with new created thing', function(){
      newProject.title.should.equal('test title');
      newProject.userId.should.equal('3445ddegghhr');
      newProject.location.should.equal('Bangalore');
    });
  });

  describe('PUT /api/project/:id/interested', function(){
    var bids;
    beforeEach(function(done){
      request(app)
        .put(`/api/projects/${newProject._id}/interested`)
        .send({
          bidderId: 'xsd34354543221',
          name: 'TEST NAME',
          profileImage: '/test.png',
          contacts: '34354353454'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res)=>{
          if(err){ return done(err); }
          bids = res.body;
          done();
        });
    });

    it('newly pushed bids should be there', function(){
      bids.should.have.property('bids');
      bids.bids.should.have.length(1)
    })
  });

  describe('PUT /api/project/:id/notinterested', function(){
    var bids;
    beforeEach(function(done){
      request(app)
        .put(`/api/projects/${newProject._id}/notinterested`)
        .send({ bidderId: 'xsd34354543221' })
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res)=>{
          if(err){
            console.log(err.message);
            return done(err); }
          bids = res.body;
          done();
        });
    });

    it('Bid list should be empty after deletion of single item', function(){
      bids.should.have.property('bids');
      bids.bids.should.have.length(0)
    })
  });

  describe('GET /api/projects/:id', function(){
    var project;

    beforeEach(function(done){
      request(app)
        .get(`/api/projects/${newProject._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res)=>{
          if(err){
            return done(err);
          }
          project = res.body;
          done();
        });
    });

    afterEach(function(){
      project = {};
    });

    it('should respond with new created thing', function(){
      project.title.should.equal('test title');
      project.userId.should.equal('3445ddegghhr');
      project.location.should.equal('Bangalore');
    });
  });


  describe('DELETE /api/projects/:id', function(){
    it('should respond with 204 on successful removal', function( done ){
      request(app)
        .delete(`/api/project/${newProject._id}`)
        .expect(404)
        .end( err => {
          if(err){
            return done(err);
          }
          done();
        })
    });

    it('should respond with 404 when project does not exist', function( done ){
      request(app)
        .delete(`/api/things/${newProject._id}`)
        .expect(404)
        .end(err => {
          if( err ){
            return done(err);
          }
          done();
        });
    });
  });

});

