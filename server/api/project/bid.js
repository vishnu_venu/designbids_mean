/**
 * Created by vishnu on 9/7/16.
 */
'use strict'
mongoose.promise = require('bluebird');
import mongoose, { Schema } from 'mongoose';

export const BidSchema = new Schema({
  bidderId: { type: String ,required: true},
  name: { type: String , required: true},
  profileImage: { type: String , required: true},
  contacts: { type: String, required: true},
  bid: Number,
  status: { type: String, default: 'INTERESTED'}
  // STATUS: INTERESTED->APPROVED|REJECT -> WON|REJECT
},{
  timestamps: { createAt: 'created_at'}
});





