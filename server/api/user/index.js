'use strict';

'use strict';

import { Router } from 'express';
import * as controller from './user.controller';
import * as auth from '../../auth/auth.service';

var router = new Router();

router.get('/', auth.hasRole('admin'), controller.index);
router.delete('/:id', auth.hasRole('admin'), controller.destroy);


router.get('/me', auth.isAuthenticated(), controller.me);
router.put('/:id/password', auth.isAuthenticated(), controller.changePassword);
router.get('/:id', auth.isAuthenticated(), controller.show);
router.post('/', controller.create);


router.get('/designers/me', auth.isAuthenticated(), controller.me);
router.get('/designers/:id', auth.isAuthenticated(), controller.show)
router.get('/designers/:id/portfolio', auth.isAuthenticated())
router.post('/designers/create/portfolio',auth.isAuthenticated())
router.delete('/designers/:id/portfolio',auth.isAuthenticated())
router.put('/designers/:id/portfolio',auth.isAuthenticated());


module.exports = router;
