/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';
import Thing from '../api/thing/thing.model';
import User from '../api/user/user.model';
import Project from '../api/project/project.model';

Thing.find({}).remove()
  .then(() => {
    Thing.create({
      name: 'Development Tools',
      info: 'Integration with popular tools such as Webpack, Gulp, Babel, TypeScript, Karma, ' +
             'Mocha, ESLint, Node Inspector, Livereload, Protractor, Pug, ' +
             'Stylus, Sass, and Less.'
    }, {
      name: 'Server and Client integration',
      info: 'Built with a powerful and fun stack: MongoDB, Express, ' +
             'AngularJS, and Node.'
    }, {
      name: 'Smart Build System',
      info: 'Build system ignores `spec` files, allowing you to keep ' +
             'tests alongside code. Automatic injection of scripts and ' +
             'styles into your index.html'
    }, {
      name: 'Modular Structure',
      info: 'Best practice client and server structures allow for more ' +
             'code reusability and maximum scalability'
    }, {
      name: 'Optimized Build',
      info: 'Build process packs up your templates as a single JavaScript ' +
             'payload, minifies your scripts/css/images, and rewrites asset ' +
             'names for caching.'
    }, {
      name: 'Deployment Ready',
      info: 'Easily deploy your app to Heroku or Openshift with the heroku ' +
             'and openshift subgenerators'
    });
  });

User.find({}).remove()
  .then(() => {
    User.create({
      provider: 'local',
      name: 'Test User',
      email: 'test@example.com',
      password: 'test'
    }, {
      provider: 'local',
      role: 'admin',
      name: 'Admin',
      email: 'admin@example.com',
      password: 'admin'
    })
    .then(() => {
      console.log('finished populating users');
    });
  });


Project.find({}).remove()
  .then(()=>{
    Project.create({
      userId: 'efe33445533',
      title: "2-BHK Home for renovate",
      description: 'I need to renovate my 2-BHK flat in HSR layout.',
      budget: 2334455,
      location: 'BANGLORE, HSR'
    },{
      userId: 'efe33445534',
      title: "3-BHK Home for renovate",
      description: 'I need to renovate my 3-BHK flat in HSR layout.',
      budget: 2334455,
      location: 'BANGLORE, HSR'
    },{
      userId: 'efe33445535',
      title: "4-BHK Home for renovate",
      description: 'I need to renovate my 4-BHK flat in HSR layout.',
      budget: 2334455,
      location: 'BANGLORE, HSR'
    },{
      userId: 'efe33445536',
      title: "5-BHK Home for renovate",
      description: 'I need to renovate my 5-BHK flat in HSR layout.',
      budget: 2334455,
      location: 'BANGLORE, HSR'
    })
  });
